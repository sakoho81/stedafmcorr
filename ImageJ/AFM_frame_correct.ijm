macro "AFM frame correct"
{
	run("Open MI");

	temp = getImageID();
	for (i=0; i<4; i++)
	{
		selectImage(temp+i);
		if (indexOf(getTitle(), "Amplitude") >= 0)
		{
			close();
			 
		}
		else
		{
			run("Bandpass Filter...", "filter_large="+getWidth()+" filter_small=0 suppress=Horizontal tolerance=5 autoscale saturate");
		}
	}
}
	