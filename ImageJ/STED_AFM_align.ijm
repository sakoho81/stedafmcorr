// Macro: STED_AFM_align
// Sami Koho, 10.03.2011, Uni. Turku (Lab of Biophysics)

// This macro implements methods for analysing data obtained from
// combined AFM-STED experiments. Original images are first tidied up and then
// the images acquired with the two different methods are overlaid using landmark data
// obtained from AFM tip position at frame corners.


// Screen size setting. Affects the way windows are positioned on screen
var screen_width = screenWidth;
var screen_height = screenHeight;

// Default thresholding/filtering settings for automatic tip position extraction
var low_thd = 	100;
var high_thd = 255;
var med_kernel = 3;
var multiplier = 2;

// Globals
var first_afm;
var last_corner;
var psf;

var modalities = newArray("STED", "Confo");

macro "AFM-STED overlay"
{
	// Debug
	doCommand("Close All");
	print("\\Clear");

	// Indtroduction text for the user
	Dialog.create("STED-AFM alignment");
	Dialog.addMessage("You will be prompted to select: \n 1. Fluorescence images for AFM tip posisiton \n 2. STED image for overlay \n 3. AFM images MI file");  
	Dialog.show();

	// Find out how what imaging modality is used. This affects to the expected
	// size of the AFM tip
	Dialog.create("Imaging mode selection");
	Dialog.addChoice("Mode", modalities);
	Dialog.show(); 
	if (Dialog.getChoice() == "STED")
	{
		psf = 50;
	}
	else
	{
		psf = 250;
	}
	print(psf);

	/*getPixelSize(unit, pw, ph);
	tip_size = multiplier*pw/psf;
	*/
	
	// FIND LANDMARKS
	// -----------------------------------------------------------------------------------------
	// Alignment begins by opening landmark AFM tip images from LIF files. At least three images should
	// be selected.
	open();
	n_corners = nImages();
	last_corner = getImageID();
	tip_pos_size = getWidth();

	
	// Organize windows on lower half of the screen. Dimensions depend on the amount of images
	// selected
	afm_tip_dims = screen_width/n_corners;
	for (i=0; i < n_corners; i++)
	{
		selectImage(last_corner+i);
		setLocation(screen_width-(i+1)*afm_tip_dims, screen_height-afm_tip_dims-50, afm_tip_dims, afm_tip_dims);
	}
	/* At least 3 corners are needed to do the alignment
	if (n_corners < 3 || n_corners > 4)
	{
		doCommand("Close All");
		print ("Not enough images are open."); 
		print ("You need to select at least 3 AFM tip images.");
		exit();
	}
*/
	x_coordinates = newArray(n_corners);
	y_coordinates = newArray(n_corners);

	try_again = false;
	
	for (i=0; i < n_corners; i++)
	{	
		if (try_again)
		{
			i=0;
			try_again = false;
		}
		// Automatically look up tip locations
		selectImage(last_corner+i);
		run("Subtract Background...", "rolling=10");
		run("Bandpass Filter...", "filter_large=10 filter_small=0 suppress=None tolerance=5");
		run("Despeckle");
		adjust = false;
		end = false;
		thd_low_tmp = low_thd;
		thd_high_tmp = high_thd;
		while (end == false)
		{
			setThreshold(thd_low_tmp,thd_high_tmp);
			run("Find Maxima...", "noise=10 output=[Point Selection] exclude above");
			if (selectionType() == -1)
			{
				thd_low_tmp = thd_low_tmp - 5;
			}
			else {
				getSelectionCoordinates(x,y);
				while (x.length > 1)
				{
					waitForUser("Select a ROI");
					getSelectionCoordinates(x,y);
						
				}
				end = true;
			}
		}
			
			/* 
			 
			 do_again = getBoolean("Failed to find afm tip positions.\n Would you like to change thresholding levels?");
			if (do_again)
			{
				Dialog.create("Thresholding settings");
				Dialog.addMessage("Insert thresholding limits: \n");
				Dialog.addNumber("Low", 170);
				Dialog.addNumber("High", 255);
				Dialog.addMessage("Insert Median kernel size: \n");
				Dialog.addNumber("Size", 3);
				Dialog.show();	
				low_thd = Dialog.getNumber();
				high_thd = Dialog.getNumber();
				med_kernel = Dialog.getNumber();
				for (j=0; j < n_corners; j++)
				{
					selectImage(last_corner+j);
					resetThreshold();
					run("Select None");
				}
				try_again = true;					
			}
			
			else
			{
				run("Close All");
				exit();
			}
		}

		*/
		x_coordinates[i]=x[0];
		y_coordinates[i]=y[0];
	}
	//----------------------------------------------------------------------------------------
	// SORT CORNERS
	// No particular order is defined for the AFM cantilever pictures, which will be used as
	// landmarks in registration. In addition if three landmarks are used (most common), selection can be made in four
	// different ways. Therefore the coordinates acquired in the previous section need
	// to be sorted:
	//              	  1*		3*
	//
	//
	//
	//			  2*		4*
	// 
	
	
	// Some constants for array indexing
	first = 0;
	second = 1;
	third = 2; 
	fourth = 3;

	// First x - coordinates are put in order	
	for (i=0; i < n_corners-1; i++)
	{
		for (j=i+1; j < n_corners; j++)
		{
			if (x_coordinates[j] < x_coordinates[i])
			{
				temp = x_coordinates[j];
				x_coordinates[j] = x_coordinates[i];
				x_coordinates[i] = temp;

				temp = y_coordinates[j];
				y_coordinates[j] = y_coordinates[i];
				y_coordinates[i] = temp;
			}
		}
	}
	
	// Left hand side corners might be in wrong order
	if (x_coordinates[first]+ x_coordinates[x_coordinates.length - 1]/2 > x_coordinates[second] && y_coordinates[second] < y_coordinates[first])
	{
		temp = x_coordinates[first];
		x_coordinates[first] = x_coordinates[second];
		x_coordinates[second] = temp;

		temp = y_coordinates[first];
		y_coordinates[first] = y_coordinates[second];
		y_coordinates[second] = temp;
	}
	// Right hand side corners might be in wrong order as well
	// Checked for 3 corners
	if (x_coordinates[first]+ x_coordinates[x_coordinates.length - 1]/2 < x_coordinates[second])
	{
		if  (y_coordinates[third] < y_coordinates[second])
		{
			temp = x_coordinates[second];
			x_coordinates[second] = x_coordinates[third];
			x_coordinates[third] = temp;

			temp = y_coordinates[second];
			y_coordinates[second] = y_coordinates[third];
			y_coordinates[third] = temp;
		}
	}
	// Checked for 4 corners
	if (n_corners == 4)
	{
		if  (y_coordinates[fourth] < y_coordinates[third])
		{
			temp = x_coordinates[third];
			x_coordinates[third] = x_coordinates[fourth];
			x_coordinates[fourth] = temp;

			temp = y_coordinates[third];
			y_coordinates[third] = y_coordinates[fourth];
			y_coordinates[fourth] = temp;
		}
	}

	// DEBUG: print coordinates
	for (i=0; i<x_coordinates.length; i++)
	{
		print(i+" "+x_coordinates[i]+" "+y_coordinates[i]);
	}

	selectWindow("Log");
	
	//--------------------------------------------------------------------------------------------------------------------------------
	// GET AFM FRAMES
	
	//run("AFM frame correct");
	
	run("Open MI");

	temp = getImageID();
	//print ("This is the ID" +last_corner-temp);
	afm_nr = last_corner-temp;
	loc_temp = 0;
	for (i=0; i<afm_nr; i++)
	{
		selectImage(temp+i);
		/*if (indexOf(getTitle(), "Amplitude") >= 0)
		{
			close();
			 
		}
		else
		{*/
			run("Bandpass Filter...", "filter_large="+getWidth()+" filter_small=0 suppress=Horizontal tolerance=5 autoscale saturate");
			run("Rotate 90 Degrees Right");
			setLocation(loc_temp*screen_width/afm_nr,50, screen_width/afm_nr, screen_width/afm_nr);
			loc_temp++;
		//}
		
	}
	waitForUser("Select AFM frame to overlay");
	first_afm = getImageID()+1;
	source_im = getTitle();
	rename("AFM_Topo_1");
	run("Fire");
	source_im_alias = getTitle();
	source_size = getWidth();
	

	if (n_corners == 3)
	{
		// Is there a landmark in lower left corner?
		if (y_coordinates[first]+ x_coordinates[third]/2 < y_coordinates[second])
		{
			// If yes. Is the right sight landmark up or down?
			// Upper right?
			if (y_coordinates[third]+ x_coordinates[third]/2 < y_coordinates[second])
			{
				source_x = newArray(0, 0, source_size-1);
				source_y = newArray(0, source_size-1, 0);
			}
			else
			{
				source_x = newArray(0, 0, source_size-1);
				source_y = newArray(0, source_size-1, source_size-1);
			}
		}
		else
		{
			source_x = newArray(0, source_size-1, source_size-1);
			source_y = newArray(source_size-1, 0, source_size-1);
		}
				
	}
	else 
	{
		source_x = newArray(0, 0, source_size-1, source_size-1);
		source_y = newArray(0, source_size-1, 0, source_size-1);
	}
	
	for (i=0; i<source_x.length; i++)
	{
		print(i+" "+source_x[i]+" "+source_y[i]);
	}
	
	// --------------------------------------------------------------------------------------------
	// GET STED IMAGE
	
	open();
	STED_id = getImageID();
	target_im = getTitle();
	rename("STED_1");
	run("32-bit");
	target_im_alias = getTitle();
	target_size=getWidth();
	
	
	// ---------------------------------------------------------------------------------------------
	// MAKE OVERLAY
	// overlay is made with TurboReg ImageJ plugin. There are many other similar ones, but this seemed to work quickly
	// and precisely
	
	//template = "-align -window "+source_im_alias+" 0 0 "+source_size-1+" "+source_size-1+" -window "+target_im_alias+ " 0 0 "+target_size-1+" "+target_size-1+" ";
	
	// Construct overlay command from landmark data
	template = "-transform -window "+source_im_alias+" "+target_size+" "+target_size+" ";
	pos_scale = target_size/tip_pos_size;

	
	if (n_corners == 3)
	{
		template = template +"-affine";
	}
	else
	{
		template = template +"-bilinear";
	}
	for (i=0; i < n_corners; i++)
	{
		template = template+" "+source_x[i]+" "+source_y[i]+" "+pos_scale*x_coordinates[i]+" "+pos_scale*y_coordinates[i];
	}
	template = template + " -showOutput";

	// print (template);

	
	// Run TurboReg and cleanup picture
	run ("TurboReg ", template);

	run("Stack to Images");
	selectWindow("Data");
	run("Enhance Contrast", "saturated=1 normalize");
	selectWindow("Mask");
	setAutoThreshold("IsoData");
	
	// Remove black borders from transformed AFM image
	imageCalculator("Multiply create 32-bit", "Data","Mask");

	selectWindow("Result of Data");

	//Close additional findows
	selectWindow("Data");
	close();
	selectWindow("Mask");
	close();

	// Merge AFM and fluorescence images
	run("Merge Channels...", "red=STED_1 green=[Result of Data] blue=*None* gray=*None* create keep");
		
}



