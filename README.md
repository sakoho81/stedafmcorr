# SteAfmCorr  
## A utility for correlative STED-AFM super-resolution microscopy
***

*StedAfmCorr* is a software tool for integrating STED and AFM microscopes for correlative experiments. The aim was to create a generic tool that could be relatively easy to modify for any given instrument combination. The *StedAfmCorr* was written in LabVIEW. There is an ImageJ script available as well, for STED-AFM image registration.

The software was developed at Laboratory of Biophysics (University of Turku, Finland). It is distributed under [BSD license](https://bitbucket.org/sakoho81/stedafmcorr/raw/383a65aa49696d5edf8a8ae89a0efb2cf3f45ca4/License.txt).

### How does it work?

It works in three modes:

1. STED-AFM correlative imaging 
2. Offline STED-AFM image registration and 
3. Live cell imaging. 

The *StedAfmCorr* controls the data acquisition in both of the instruments and performs the registration of the STED and AFM images. AFM frame corners are utilised as landmarks for registration. In mode (3) AFM is used for stimulating the sample instead of imaging; the data acquisition in STED is synchronized to the stimualtion events through *StedAfmCorr*

### How do I get set up? ###

*StedAfmCorr* currently works with a commercial Leica TCS STED microscope and a Agilent 5500ilm AFM. The software runs on the AFM computer - it can control the STED data acquisition through hardware trigger signals. AFM is controlled through AFM PicoScript interface.

In order to make the sotware tool compatible with other kinds of instruments as well, the LabVIEW code was written using generic (OOP) programming practices. Both STED and AFM instruments are implemented as generic class templates; LabVIEW Dynamic Dispatch is used to switch between instrument models. In order to make the *StedAfmCorr* on other systems, one only needs to implement the STED and/or AFM drivers, based on existing templates.

*StedAfmCorr* requires the AFM to be controllable through a scripting interface. STED must be controllable, at least, through hardware trigger signals (as in Leica instruments) - a more complete control would of course be beneficial.

### Contribution guidelines ###

The *StedAfmCorr* is distributed under BSD open-source license. You can use the software in any way you like; we would just ask you to aknowledge our work, as specified in the license file.

All kinds of contributions, in forms of additional instrument drivers, registration algorithms, live imaging applications etc. are welcome! 

### Contacts ###

Sami Koho <sami.koho@gmail.com>
